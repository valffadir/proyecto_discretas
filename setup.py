import sys
from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = {
    "packages": ["os"],
    "excludes": ["matplotlib.tests","numpy.random._examples"],
    "includes": ["atexit"],
    "zip_include_packages": ["PyQt5"]
}

base = None
if sys.platform == "win32":
    base = "Win32GUI"

setup(name="main_window",
    version = "1.1",
    description = "Analiza las propiedades de una relacion aRb y muestra un grafo para R",
    options = {"build_exe": build_exe_options},
    executables = [Executable("main_window.py", icon="icon.png", base=base)])
