#re.sub('[^A-Za-z0-9,]+','', R)
#R1[i] = eval(re.sub('[^A-Za-z0-9,]+', '' , R1[i]))  #en tuplas
#self.label_1.setVisible(False)


import regex as re

def format_relation(relation):
    relation = re.sub('[^A-Za-z0-9,()]+', '' , relation).replace("),(",")_(").split("_")
    for i in range (len(relation)):
        relation[i] = tuple(re.sub('[^A-Za-z0-9,]+', '' , relation[i]).split(',')) #en strings
    relation = set(relation)    #se convierte la lista en set para evitar pares repetidos
    return relation

def format_set(set_a):
    A = set(re.sub('[^A-Za-z0-9,]+','', set_a).split(","))
    return A

def is_reflexive(A,R):
    res = dict()
    res['reflex'] = 1  #si al final del ciclo queda en 1, si es reflexiva
    res['faltan'] = set()   #guarda las tuplas que faltan para ser reflex
    for element in A:
        par = tuple( (element,element) )
        if par not in R :
            res['reflex'] = 0
            res['faltan'].add(par)
    return res

def is_symmetric(A,R):
    res = dict()
    res['sim'] = 1  #si al final del ciclo queda en 1, si es reflexiva
    res['faltan'] = set()   #guarda las tuplas que faltan para ser reflex
    for par in R:
        tupla = tuple( (par[1], par[0]) )
        if tupla not in R:
            res['sim'] = 0
            res['faltan'].add(tupla)
    return res

def is_antisymmetric(A,R):
    res = dict()
    res['anti'] = 1  #si al final del ciclo queda en 1, si es reflexiva
    res['sobran'] = set()   #guarda las tuplas que faltan para ser reflex
    for par in R:
        tupla = tuple( (par[1], par[0]) )
        if tupla in R:
            if par[1] != par[0]:
                res['anti'] = 0
                res['sobran'].add(tupla)
    return res


A = "{1,2,3}"
R = '{(1,1),(1,2),(1,3),(3,1),(2,2),(3,3), (1,1)}'
Rr = '{(1,1),(1,2),(1,3),(3,1),(2,2),(3,3),(3,3)}'
Rs = '{(1,1),(1,2),(1,3),(3,1),(2,2),(2,1)}'
Rt = '{(1,1),(1,2),(1,3),(3,1),(2,2)}'

A = format_set(A)
R1 = format_relation(Rr)    #se formatea la relacion en un set de tuplas

prop_reflex = is_reflexive(A,R1);   #devuelve un diccionario, con 'reflex' y 'faltan'
prop_sim = is_symmetric(A,R1);   #devuelve un diccionario, con 'sim' y 'faltan'
prop_anti = is_antisymmetric(A,R1);   #devuelve un diccionario, con 'anti' y 'sobran'


        
