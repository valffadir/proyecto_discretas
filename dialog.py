# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'main_window.ui'
#
# Created by: PyQt5 UI code generator 5.15.1
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(851, 509)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setEnabled(True)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setContentsMargins(10, 10, 10, 0)
        self.gridLayout.setSpacing(8)
        self.gridLayout.setObjectName("gridLayout")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 2, 0, 1, 1)
        self.lineA = QtWidgets.QLineEdit(self.centralwidget)
        self.lineA.setObjectName("lineA")
        self.gridLayout.addWidget(self.lineA, 1, 2, 1, 1)
        self.label_10 = QtWidgets.QLabel(self.centralwidget)
        self.label_10.setObjectName("label_10")
        self.gridLayout.addWidget(self.label_10, 0, 0, 1, 4)
        self.label_op = QtWidgets.QLabel(self.centralwidget)
        self.label_op.setText("")
        self.label_op.setObjectName("label_op")
        self.gridLayout.addWidget(self.label_op, 13, 2, 1, 1)
        self.label_reflex = QtWidgets.QLabel(self.centralwidget)
        self.label_reflex.setText("")
        self.label_reflex.setObjectName("label_reflex")
        self.gridLayout.addWidget(self.label_reflex, 6, 2, 1, 1)
        self.label_9 = QtWidgets.QLabel(self.centralwidget)
        self.label_9.setObjectName("label_9")
        self.gridLayout.addWidget(self.label_9, 2, 3, 1, 1)
        self.label_head1 = QtWidgets.QLabel(self.centralwidget)
        self.label_head1.setEnabled(False)
        self.label_head1.setObjectName("label_head1")
        self.gridLayout.addWidget(self.label_head1, 5, 0, 1, 3)
        self.checkBoxOP = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBoxOP.setEnabled(False)
        self.checkBoxOP.setCursor(QtGui.QCursor(QtCore.Qt.ForbiddenCursor))
        self.checkBoxOP.setCheckable(False)
        self.checkBoxOP.setObjectName("checkBoxOP")
        self.gridLayout.addWidget(self.checkBoxOP, 13, 0, 1, 1)
        self.checkBoxAnti = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBoxAnti.setEnabled(False)
        self.checkBoxAnti.setCursor(QtGui.QCursor(QtCore.Qt.ForbiddenCursor))
        self.checkBoxAnti.setCheckable(False)
        self.checkBoxAnti.setObjectName("checkBoxAnti")
        self.gridLayout.addWidget(self.checkBoxAnti, 9, 0, 1, 1)
        self.label_7 = QtWidgets.QLabel(self.centralwidget)
        self.label_7.setObjectName("label_7")
        self.gridLayout.addWidget(self.label_7, 2, 1, 1, 1)
        self.label_head3 = QtWidgets.QLabel(self.centralwidget)
        self.label_head3.setEnabled(False)
        self.label_head3.setObjectName("label_head3")
        self.gridLayout.addWidget(self.label_head3, 15, 0, 1, 3)
        self.label_head2 = QtWidgets.QLabel(self.centralwidget)
        self.label_head2.setEnabled(False)
        self.label_head2.setObjectName("label_head2")
        self.gridLayout.addWidget(self.label_head2, 11, 0, 1, 3)
        self.checkBoxEq = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBoxEq.setEnabled(False)
        self.checkBoxEq.setCursor(QtGui.QCursor(QtCore.Qt.ForbiddenCursor))
        self.checkBoxEq.setCheckable(False)
        self.checkBoxEq.setObjectName("checkBoxEq")
        self.gridLayout.addWidget(self.checkBoxEq, 12, 0, 1, 1)
        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setObjectName("label_6")
        self.gridLayout.addWidget(self.label_6, 1, 1, 1, 1)
        self.checkBoxReflex = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBoxReflex.setEnabled(False)
        self.checkBoxReflex.setCursor(QtGui.QCursor(QtCore.Qt.ForbiddenCursor))
        self.checkBoxReflex.setCheckable(False)
        self.checkBoxReflex.setObjectName("checkBoxReflex")
        self.gridLayout.addWidget(self.checkBoxReflex, 6, 0, 1, 1)
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 1, 0, 1, 1)
        self.checkBoxSim = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBoxSim.setEnabled(False)
        self.checkBoxSim.setCursor(QtGui.QCursor(QtCore.Qt.ForbiddenCursor))
        self.checkBoxSim.setCheckable(False)
        self.checkBoxSim.setObjectName("checkBoxSim")
        self.gridLayout.addWidget(self.checkBoxSim, 8, 0, 1, 1)
        self.line_3 = QtWidgets.QFrame(self.centralwidget)
        self.line_3.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_3.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_3.setObjectName("line_3")
        self.gridLayout.addWidget(self.line_3, 14, 0, 1, 4)
        self.checkBoxTrans = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBoxTrans.setEnabled(False)
        self.checkBoxTrans.setCursor(QtGui.QCursor(QtCore.Qt.ForbiddenCursor))
        self.checkBoxTrans.setCheckable(False)
        self.checkBoxTrans.setObjectName("checkBoxTrans")
        self.gridLayout.addWidget(self.checkBoxTrans, 7, 0, 1, 1)
        self.line = QtWidgets.QFrame(self.centralwidget)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.gridLayout.addWidget(self.line, 4, 0, 1, 4)
        self.buttonIdentificar = QtWidgets.QPushButton(self.centralwidget)
        self.buttonIdentificar.setObjectName("buttonIdentificar")
        self.gridLayout.addWidget(self.buttonIdentificar, 3, 1, 1, 3)
        self.label_8 = QtWidgets.QLabel(self.centralwidget)
        self.label_8.setObjectName("label_8")
        self.gridLayout.addWidget(self.label_8, 1, 3, 1, 1)
        self.label_sim = QtWidgets.QLabel(self.centralwidget)
        self.label_sim.setText("")
        self.label_sim.setObjectName("label_sim")
        self.gridLayout.addWidget(self.label_sim, 8, 2, 1, 1)
        self.lineR = QtWidgets.QLineEdit(self.centralwidget)
        self.lineR.setObjectName("lineR")
        self.gridLayout.addWidget(self.lineR, 2, 2, 1, 1)
        self.label_trans = QtWidgets.QLabel(self.centralwidget)
        self.label_trans.setText("")
        self.label_trans.setObjectName("label_trans")
        self.gridLayout.addWidget(self.label_trans, 7, 2, 1, 1)
        self.label_anti = QtWidgets.QLabel(self.centralwidget)
        self.label_anti.setText("")
        self.label_anti.setObjectName("label_anti")
        self.gridLayout.addWidget(self.label_anti, 9, 2, 1, 1)
        self.label_eq = QtWidgets.QLabel(self.centralwidget)
        self.label_eq.setText("")
        self.label_eq.setObjectName("label_eq")
        self.gridLayout.addWidget(self.label_eq, 12, 2, 1, 1)
        self.textEdit_part = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit_part.setEnabled(True)
        self.textEdit_part.setMaximumSize(QtCore.QSize(430, 16777215))
        self.textEdit_part.setSizeIncrement(QtCore.QSize(2, 0))
        self.textEdit_part.setReadOnly(True)
        self.textEdit_part.setObjectName("textEdit_part")
        self.gridLayout.addWidget(self.textEdit_part, 16, 0, 1, 3)
        self.line_2 = QtWidgets.QFrame(self.centralwidget)
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.gridLayout.addWidget(self.line_2, 10, 0, 1, 4)
        self.graphicsView = QtWidgets.QGraphicsView(self.centralwidget)
        self.graphicsView.setMinimumSize(QtCore.QSize(362, 419))
        self.graphicsView.setObjectName("graphicsView")
        self.gridLayout.addWidget(self.graphicsView, 1, 4, 16, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 851, 21))
        self.menubar.setObjectName("menubar")
        self.menuEjemplos = QtWidgets.QMenu(self.menubar)
        self.menuEjemplos.setObjectName("menuEjemplos")
        self.menuRelaciones_de_equivalencia = QtWidgets.QMenu(self.menuEjemplos)
        self.menuRelaciones_de_equivalencia.setObjectName("menuRelaciones_de_equivalencia")
        self.menuRelaciones_de_orden_parcial = QtWidgets.QMenu(self.menuEjemplos)
        self.menuRelaciones_de_orden_parcial.setObjectName("menuRelaciones_de_orden_parcial")
        self.menuRelaciones_varias = QtWidgets.QMenu(self.menuEjemplos)
        self.menuRelaciones_varias.setObjectName("menuRelaciones_varias")
        self.menuAyuda = QtWidgets.QMenu(self.menubar)
        self.menuAyuda.setObjectName("menuAyuda")
        self.menuOpciones = QtWidgets.QMenu(self.menubar)
        self.menuOpciones.setObjectName("menuOpciones")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setEnabled(True)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionAcerca_de = QtWidgets.QAction(MainWindow)
        self.actionAcerca_de.setObjectName("actionAcerca_de")
        self.actionLmpiar = QtWidgets.QAction(MainWindow)
        self.actionLmpiar.setObjectName("actionLmpiar")
        self.actionSalir = QtWidgets.QAction(MainWindow)
        self.actionSalir.setObjectName("actionSalir")
        self.actionEjEq_1 = QtWidgets.QAction(MainWindow)
        self.actionEjEq_1.setObjectName("actionEjEq_1")
        self.actionEjEq_2 = QtWidgets.QAction(MainWindow)
        self.actionEjEq_2.setObjectName("actionEjEq_2")
        self.actionEjEq_3 = QtWidgets.QAction(MainWindow)
        self.actionEjEq_3.setObjectName("actionEjEq_3")
        self.actionEjOP_1 = QtWidgets.QAction(MainWindow)
        self.actionEjOP_1.setObjectName("actionEjOP_1")
        self.actionEjOP_2 = QtWidgets.QAction(MainWindow)
        self.actionEjOP_2.setObjectName("actionEjOP_2")
        self.actionEjX_1 = QtWidgets.QAction(MainWindow)
        self.actionEjX_1.setObjectName("actionEjX_1")
        self.actionEjX_2 = QtWidgets.QAction(MainWindow)
        self.actionEjX_2.setObjectName("actionEjX_2")
        self.actionEjX_3 = QtWidgets.QAction(MainWindow)
        self.actionEjX_3.setObjectName("actionEjX_3")
        self.actionEjOP_3 = QtWidgets.QAction(MainWindow)
        self.actionEjOP_3.setObjectName("actionEjOP_3")
        self.menuRelaciones_de_equivalencia.addAction(self.actionEjEq_1)
        self.menuRelaciones_de_equivalencia.addAction(self.actionEjEq_2)
        self.menuRelaciones_de_equivalencia.addAction(self.actionEjEq_3)
        self.menuRelaciones_de_orden_parcial.addAction(self.actionEjOP_1)
        self.menuRelaciones_de_orden_parcial.addAction(self.actionEjOP_2)
        self.menuRelaciones_de_orden_parcial.addAction(self.actionEjOP_3)
        self.menuRelaciones_varias.addAction(self.actionEjX_1)
        self.menuRelaciones_varias.addAction(self.actionEjX_2)
        self.menuRelaciones_varias.addAction(self.actionEjX_3)
        self.menuEjemplos.addAction(self.menuRelaciones_de_equivalencia.menuAction())
        self.menuEjemplos.addAction(self.menuRelaciones_de_orden_parcial.menuAction())
        self.menuEjemplos.addAction(self.menuRelaciones_varias.menuAction())
        self.menuAyuda.addAction(self.actionAcerca_de)
        self.menuOpciones.addAction(self.actionLmpiar)
        self.menuOpciones.addSeparator()
        self.menuOpciones.addAction(self.actionSalir)
        self.menubar.addAction(self.menuOpciones.menuAction())
        self.menubar.addAction(self.menuEjemplos.menuAction())
        self.menubar.addAction(self.menuAyuda.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        MainWindow.setTabOrder(self.lineA, self.lineR)
        MainWindow.setTabOrder(self.lineR, self.buttonIdentificar)
        MainWindow.setTabOrder(self.buttonIdentificar, self.checkBoxReflex)
        MainWindow.setTabOrder(self.checkBoxReflex, self.checkBoxTrans)
        MainWindow.setTabOrder(self.checkBoxTrans, self.checkBoxSim)
        MainWindow.setTabOrder(self.checkBoxSim, self.checkBoxAnti)
        MainWindow.setTabOrder(self.checkBoxAnti, self.checkBoxEq)
        MainWindow.setTabOrder(self.checkBoxEq, self.checkBoxOP)
        MainWindow.setTabOrder(self.checkBoxOP, self.textEdit_part)
        MainWindow.setTabOrder(self.textEdit_part, self.graphicsView)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Identificador de Relaciones - Proyecto 1"))
        self.label_2.setText(_translate("MainWindow", "Relación R="))
        self.lineA.setToolTip(_translate("MainWindow", "<html><head/><body><p>ej. 1,2,3</p></body></html>"))
        self.label_10.setText(_translate("MainWindow", "Sea A un conjunto de elementos y R una relación aRa, tal que a∈A. Defina:"))
        self.label_9.setText(_translate("MainWindow", "}"))
        self.label_head1.setText(_translate("MainWindow", "La Relación R tiene las siguientes propiedades:"))
        self.checkBoxOP.setText(_translate("MainWindow", "De Orden Parcial"))
        self.checkBoxAnti.setText(_translate("MainWindow", "Antisimétrica"))
        self.label_7.setText(_translate("MainWindow", "{"))
        self.label_head3.setText(_translate("MainWindow", "Ya que es una relación de Equivalencia, es posible determinar las particiones de R:"))
        self.label_head2.setText(_translate("MainWindow", "Por lo tanto, es una relación:"))
        self.checkBoxEq.setText(_translate("MainWindow", "De Equivalencia"))
        self.label_6.setText(_translate("MainWindow", "{"))
        self.checkBoxReflex.setText(_translate("MainWindow", "Reflexiva"))
        self.label.setText(_translate("MainWindow", "Elementos en A = "))
        self.checkBoxSim.setText(_translate("MainWindow", "Simétrica"))
        self.checkBoxTrans.setText(_translate("MainWindow", "Transitiva"))
        self.buttonIdentificar.setText(_translate("MainWindow", "Identificar propiedades de R"))
        self.label_8.setText(_translate("MainWindow", "}"))
        self.menuEjemplos.setTitle(_translate("MainWindow", "Ejemplos"))
        self.menuRelaciones_de_equivalencia.setTitle(_translate("MainWindow", "Relaciones de equivalencia"))
        self.menuRelaciones_de_orden_parcial.setTitle(_translate("MainWindow", "Relaciones de orden parcial"))
        self.menuRelaciones_varias.setTitle(_translate("MainWindow", "Relaciones varias"))
        self.menuAyuda.setTitle(_translate("MainWindow", "Ayuda"))
        self.menuOpciones.setTitle(_translate("MainWindow", "Opciones"))
        self.actionAcerca_de.setText(_translate("MainWindow", "Acerca de..."))
        self.actionLmpiar.setText(_translate("MainWindow", "Limpiar"))
        self.actionSalir.setText(_translate("MainWindow", "Salir"))
        self.actionEjEq_1.setText(_translate("MainWindow", "Ejemplo 1"))
        self.actionEjEq_2.setText(_translate("MainWindow", "Ejemplo 2"))
        self.actionEjEq_3.setText(_translate("MainWindow", "Ejemplo 3"))
        self.actionEjOP_1.setText(_translate("MainWindow", "Ejemplo 1"))
        self.actionEjOP_2.setText(_translate("MainWindow", "Ejemplo 2"))
        self.actionEjX_1.setText(_translate("MainWindow", "Ejemplo 1"))
        self.actionEjX_2.setText(_translate("MainWindow", "Ejemplo 2"))
        self.actionEjX_3.setText(_translate("MainWindow", "Ejemplo 3"))
        self.actionEjOP_3.setText(_translate("MainWindow", "Ejemplo 3"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
