from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(851, 509)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setEnabled(True)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setContentsMargins(10, 10, 10, 0)
        self.gridLayout.setSpacing(8)
        self.gridLayout.setObjectName("gridLayout")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 2, 0, 1, 1)
        self.lineA = QtWidgets.QLineEdit(self.centralwidget)
        self.lineA.setObjectName("lineA")
        self.gridLayout.addWidget(self.lineA, 1, 2, 1, 1)
        self.label_10 = QtWidgets.QLabel(self.centralwidget)
        self.label_10.setObjectName("label_10")
        self.gridLayout.addWidget(self.label_10, 0, 0, 1, 4)
        self.label_op = QtWidgets.QLabel(self.centralwidget)
        self.label_op.setText("")
        self.label_op.setObjectName("label_op")
        self.gridLayout.addWidget(self.label_op, 13, 2, 1, 1)
        self.label_reflex = QtWidgets.QLabel(self.centralwidget)
        self.label_reflex.setText("")
        self.label_reflex.setObjectName("label_reflex")
        self.gridLayout.addWidget(self.label_reflex, 6, 2, 1, 1)
        self.label_9 = QtWidgets.QLabel(self.centralwidget)
        self.label_9.setObjectName("label_9")
        self.gridLayout.addWidget(self.label_9, 2, 3, 1, 1)
        self.label_head1 = QtWidgets.QLabel(self.centralwidget)
        self.label_head1.setEnabled(False)
        self.label_head1.setObjectName("label_head1")
        self.gridLayout.addWidget(self.label_head1, 5, 0, 1, 3)
        self.checkBoxOP = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBoxOP.setEnabled(False)
        self.checkBoxOP.setCursor(QtGui.QCursor(QtCore.Qt.ForbiddenCursor))
        self.checkBoxOP.setCheckable(False)
        self.checkBoxOP.setObjectName("checkBoxOP")
        self.gridLayout.addWidget(self.checkBoxOP, 13, 0, 1, 1)
        self.checkBoxAnti = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBoxAnti.setEnabled(False)
        self.checkBoxAnti.setCursor(QtGui.QCursor(QtCore.Qt.ForbiddenCursor))
        self.checkBoxAnti.setCheckable(False)
        self.checkBoxAnti.setObjectName("checkBoxAnti")
        self.gridLayout.addWidget(self.checkBoxAnti, 9, 0, 1, 1)
        self.label_7 = QtWidgets.QLabel(self.centralwidget)
        self.label_7.setObjectName("label_7")
        self.gridLayout.addWidget(self.label_7, 2, 1, 1, 1)
        self.label_head3 = QtWidgets.QLabel(self.centralwidget)
        self.label_head3.setEnabled(False)
        self.label_head3.setObjectName("label_head3")
        self.gridLayout.addWidget(self.label_head3, 15, 0, 1, 3)
        self.label_head2 = QtWidgets.QLabel(self.centralwidget)
        self.label_head2.setEnabled(False)
        self.label_head2.setObjectName("label_head2")
        self.gridLayout.addWidget(self.label_head2, 11, 0, 1, 3)
        self.checkBoxEq = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBoxEq.setEnabled(False)
        self.checkBoxEq.setCursor(QtGui.QCursor(QtCore.Qt.ForbiddenCursor))
        self.checkBoxEq.setCheckable(False)
        self.checkBoxEq.setObjectName("checkBoxEq")
        self.gridLayout.addWidget(self.checkBoxEq, 12, 0, 1, 1)
        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setObjectName("label_6")
        self.gridLayout.addWidget(self.label_6, 1, 1, 1, 1)
        self.checkBoxReflex = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBoxReflex.setEnabled(False)
        self.checkBoxReflex.setCursor(QtGui.QCursor(QtCore.Qt.ForbiddenCursor))
        self.checkBoxReflex.setCheckable(False)
        self.checkBoxReflex.setObjectName("checkBoxReflex")
        self.gridLayout.addWidget(self.checkBoxReflex, 6, 0, 1, 1)
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 1, 0, 1, 1)
        self.checkBoxSim = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBoxSim.setEnabled(False)
        self.checkBoxSim.setCursor(QtGui.QCursor(QtCore.Qt.ForbiddenCursor))
        self.checkBoxSim.setCheckable(False)
        self.checkBoxSim.setObjectName("checkBoxSim")
        self.gridLayout.addWidget(self.checkBoxSim, 8, 0, 1, 1)
        self.line_3 = QtWidgets.QFrame(self.centralwidget)
        self.line_3.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_3.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_3.setObjectName("line_3")
        self.gridLayout.addWidget(self.line_3, 14, 0, 1, 4)
        self.checkBoxTrans = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBoxTrans.setEnabled(False)
        self.checkBoxTrans.setCursor(QtGui.QCursor(QtCore.Qt.ForbiddenCursor))
        self.checkBoxTrans.setCheckable(False)
        self.checkBoxTrans.setObjectName("checkBoxTrans")
        self.gridLayout.addWidget(self.checkBoxTrans, 7, 0, 1, 1)
        self.line = QtWidgets.QFrame(self.centralwidget)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.gridLayout.addWidget(self.line, 4, 0, 1, 4)
        self.buttonIdentificar = QtWidgets.QPushButton(self.centralwidget)
        self.buttonIdentificar.setObjectName("buttonIdentificar")
        self.gridLayout.addWidget(self.buttonIdentificar, 3, 1, 1, 3)
        self.label_8 = QtWidgets.QLabel(self.centralwidget)
        self.label_8.setObjectName("label_8")
        self.gridLayout.addWidget(self.label_8, 1, 3, 1, 1)
        self.label_sim = QtWidgets.QLabel(self.centralwidget)
        self.label_sim.setText("")
        self.label_sim.setObjectName("label_sim")
        self.gridLayout.addWidget(self.label_sim, 8, 2, 1, 1)
        self.lineR = QtWidgets.QLineEdit(self.centralwidget)
        self.lineR.setObjectName("lineR")
        self.gridLayout.addWidget(self.lineR, 2, 2, 1, 1)
        self.label_trans = QtWidgets.QLabel(self.centralwidget)
        self.label_trans.setText("")
        self.label_trans.setObjectName("label_trans")
        self.gridLayout.addWidget(self.label_trans, 7, 2, 1, 1)
        self.label_anti = QtWidgets.QLabel(self.centralwidget)
        self.label_anti.setText("")
        self.label_anti.setObjectName("label_anti")
        self.gridLayout.addWidget(self.label_anti, 9, 2, 1, 1)
        self.label_eq = QtWidgets.QLabel(self.centralwidget)
        self.label_eq.setText("")
        self.label_eq.setObjectName("label_eq")
        self.gridLayout.addWidget(self.label_eq, 12, 2, 1, 1)
        self.textEdit_part = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit_part.setEnabled(True)
        self.textEdit_part.setReadOnly(True)
        self.textEdit_part.setObjectName("textEdit_part")
        self.gridLayout.addWidget(self.textEdit_part, 16, 0, 1, 3)
        self.line_2 = QtWidgets.QFrame(self.centralwidget)
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.gridLayout.addWidget(self.line_2, 10, 0, 1, 4)
        self.graphicsView = QtWidgets.QGraphicsView(self.centralwidget)
        self.graphicsView.setMinimumSize(QtCore.QSize(362, 419))
        self.graphicsView.setObjectName("graphicsView")
        self.gridLayout.addWidget(self.graphicsView, 1, 4, 16, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 851, 21))
        self.menubar.setObjectName("menubar")
        self.menuEjemplos = QtWidgets.QMenu(self.menubar)
        self.menuEjemplos.setObjectName("menuEjemplos")
        self.menuRelaciones_de_equivalencia = QtWidgets.QMenu(
            self.menuEjemplos)
        self.menuRelaciones_de_equivalencia.setObjectName(
            "menuRelaciones_de_equivalencia")
        self.menuRelaciones_de_orden_parcial = QtWidgets.QMenu(
            self.menuEjemplos)
        self.menuRelaciones_de_orden_parcial.setObjectName(
            "menuRelaciones_de_orden_parcial")
        self.menuRelaciones_varias = QtWidgets.QMenu(self.menuEjemplos)
        self.menuRelaciones_varias.setObjectName("menuRelaciones_varias")
        self.menuAyuda = QtWidgets.QMenu(self.menubar)
        self.menuAyuda.setObjectName("menuAyuda")
        self.menuOpciones = QtWidgets.QMenu(self.menubar)
        self.menuOpciones.setObjectName("menuOpciones")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setEnabled(True)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionAcerca_de = QtWidgets.QAction(MainWindow)
        self.actionAcerca_de.setObjectName("actionAcerca_de")
        self.actionLmpiar = QtWidgets.QAction(MainWindow)
        self.actionLmpiar.setObjectName("actionLmpiar")
        self.actionSalir = QtWidgets.QAction(MainWindow)
        self.actionSalir.setObjectName("actionSalir")
        self.actionEjEq_1 = QtWidgets.QAction(MainWindow)
        self.actionEjEq_1.setObjectName("actionEjEq_1")
        self.actionEjEq_2 = QtWidgets.QAction(MainWindow)
        self.actionEjEq_2.setObjectName("actionEjEq_2")
        self.actionEjEq_3 = QtWidgets.QAction(MainWindow)
        self.actionEjEq_3.setObjectName("actionEjEq_3")
        self.actionEjOP_1 = QtWidgets.QAction(MainWindow)
        self.actionEjOP_1.setObjectName("actionEjOP_1")
        self.actionEjOP_2 = QtWidgets.QAction(MainWindow)
        self.actionEjOP_2.setObjectName("actionEjOP_2")
        self.actionEjX_1 = QtWidgets.QAction(MainWindow)
        self.actionEjX_1.setObjectName("actionEjX_1")
        self.actionEjX_2 = QtWidgets.QAction(MainWindow)
        self.actionEjX_2.setObjectName("actionEjX_2")
        self.actionEjX_3 = QtWidgets.QAction(MainWindow)
        self.actionEjX_3.setObjectName("actionEjX_3")
        self.actionEjOP_3 = QtWidgets.QAction(MainWindow)
        self.actionEjOP_3.setObjectName("actionEjOP_3")
        self.menuRelaciones_de_equivalencia.addAction(self.actionEjEq_1)
        self.menuRelaciones_de_equivalencia.addAction(self.actionEjEq_2)
        self.menuRelaciones_de_equivalencia.addAction(self.actionEjEq_3)
        self.menuRelaciones_de_orden_parcial.addAction(self.actionEjOP_1)
        self.menuRelaciones_de_orden_parcial.addAction(self.actionEjOP_2)
        self.menuRelaciones_de_orden_parcial.addAction(self.actionEjOP_3)
        self.menuRelaciones_varias.addAction(self.actionEjX_1)
        self.menuRelaciones_varias.addAction(self.actionEjX_2)
        self.menuRelaciones_varias.addAction(self.actionEjX_3)
        self.menuEjemplos.addAction(
            self.menuRelaciones_de_equivalencia.menuAction())
        self.menuEjemplos.addAction(
            self.menuRelaciones_de_orden_parcial.menuAction())
        self.menuEjemplos.addAction(self.menuRelaciones_varias.menuAction())
        self.menuAyuda.addAction(self.actionAcerca_de)
        self.menuOpciones.addAction(self.actionLmpiar)
        self.menuOpciones.addSeparator()
        self.menuOpciones.addAction(self.actionSalir)
        self.menubar.addAction(self.menuOpciones.menuAction())
        self.menubar.addAction(self.menuEjemplos.menuAction())
        self.menubar.addAction(self.menuAyuda.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        MainWindow.setTabOrder(self.lineA, self.lineR)
        MainWindow.setTabOrder(self.lineR, self.buttonIdentificar)
        MainWindow.setTabOrder(self.buttonIdentificar, self.checkBoxReflex)
        MainWindow.setTabOrder(self.checkBoxReflex, self.checkBoxTrans)
        MainWindow.setTabOrder(self.checkBoxTrans, self.checkBoxSim)
        MainWindow.setTabOrder(self.checkBoxSim, self.checkBoxAnti)
        MainWindow.setTabOrder(self.checkBoxAnti, self.checkBoxEq)
        MainWindow.setTabOrder(self.checkBoxEq, self.checkBoxOP)
        MainWindow.setTabOrder(self.checkBoxOP, self.textEdit_part)
        MainWindow.setTabOrder(self.textEdit_part, self.graphicsView)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate(
            "MainWindow", "Identificador de Relaciones - Proyecto 1"))
        self.label_2.setText(_translate("MainWindow", "Relación R="))
        self.lineA.setToolTip(_translate(
            "MainWindow", "<html><head/><body><p>ej. 1,2,3</p></body></html>"))
        self.label_10.setText(_translate(
            "MainWindow", "Sea A un conjunto de elementos y R una relación aRa, tal que a∈A. Defina:"))
        self.label_9.setText(_translate("MainWindow", "}"))
        self.label_head1.setText(_translate(
            "MainWindow", "La Relación R tiene las siguientes propiedades:"))
        self.checkBoxOP.setText(_translate("MainWindow", "De Orden Parcial"))
        self.checkBoxAnti.setText(_translate("MainWindow", "Antisimétrica"))
        self.label_7.setText(_translate("MainWindow", "{"))
        self.label_head3.setText(_translate(
            "MainWindow", "Ya que es una relación de Equivalencia, es posible determinar las particiones de R:"))
        self.label_head2.setText(_translate(
            "MainWindow", "Por lo tanto, es una relación:"))
        self.checkBoxEq.setText(_translate("MainWindow", "De Equivalencia"))
        self.label_6.setText(_translate("MainWindow", "{"))
        self.checkBoxReflex.setText(_translate("MainWindow", "Reflexiva"))
        self.label.setText(_translate("MainWindow", "Elementos en A = "))
        self.checkBoxSim.setText(_translate("MainWindow", "Simétrica"))
        self.checkBoxTrans.setText(_translate("MainWindow", "Transitiva"))
        self.buttonIdentificar.setText(_translate(
            "MainWindow", "Identificar propiedades de R"))
        self.label_8.setText(_translate("MainWindow", "}"))
        self.menuEjemplos.setTitle(_translate("MainWindow", "Ejemplos"))
        self.menuRelaciones_de_equivalencia.setTitle(
            _translate("MainWindow", "Relaciones de equivalencia"))
        self.menuRelaciones_de_orden_parcial.setTitle(
            _translate("MainWindow", "Relaciones de orden parcial"))
        self.menuRelaciones_varias.setTitle(
            _translate("MainWindow", "Relaciones varias"))
        self.menuAyuda.setTitle(_translate("MainWindow", "Ayuda"))
        self.menuOpciones.setTitle(_translate("MainWindow", "Opciones"))
        self.actionAcerca_de.setText(_translate("MainWindow", "Acerca de..."))
        self.actionLmpiar.setText(_translate("MainWindow", "Limpiar"))
        self.actionSalir.setText(_translate("MainWindow", "Salir"))
        self.actionEjEq_1.setText(_translate("MainWindow", "Ejemplo 1"))
        self.actionEjEq_2.setText(_translate("MainWindow", "Ejemplo 2"))
        self.actionEjEq_3.setText(_translate("MainWindow", "Ejemplo 3"))
        self.actionEjOP_1.setText(_translate("MainWindow", "Ejemplo Hasse 1"))
        self.actionEjOP_2.setText(_translate("MainWindow", "Ejemplo Hasse 2"))
        self.actionEjX_1.setText(_translate("MainWindow", "Ejemplo 1"))
        self.actionEjX_2.setText(_translate("MainWindow", "Ejemplo 2"))
        self.actionEjX_3.setText(_translate("MainWindow", "Ejemplo 3"))
        self.actionEjOP_3.setText(_translate(
            "MainWindow", "Ejemplo Retícula 1"))


def clean_scr():
    ui.statusbar.showMessage(
        "Ingrese elementos en A y tuplas en R separadas por comas, o escoja un ejemplo precargado.")
    ui.lineA.setText("")
    ui.lineR.setText("")
    ui.label_head1.setEnabled(False)
    ui.label_head2.setEnabled(False)
    ui.label_head3.setText(
        "Analice la relación R para determinar si es de Equivalencia, de Orden parcial o ninguna de estas.")
    ui.label_head3.setEnabled(False)
    ui.checkBoxReflex.setChecked(False)
    ui.checkBoxReflex.setEnabled(False)
    ui.checkBoxSim.setChecked(False)
    ui.checkBoxSim.setEnabled(False)
    ui.checkBoxTrans.setChecked(False)
    ui.checkBoxTrans.setEnabled(False)
    ui.checkBoxAnti.setChecked(False)
    ui.checkBoxAnti.setEnabled(False)
    ui.checkBoxEq.setChecked(False)
    ui.checkBoxEq.setEnabled(False)
    ui.checkBoxOP.setChecked(False)
    ui.checkBoxOP.setEnabled(False)
    ui.label_reflex.setText("")
    ui.label_sim.setText("")
    ui.label_trans.setText("")
    ui.label_anti.setText("")
    ui.label_eq.setText("")
    ui.label_op.setText("")
    ui.textEdit_part.setText("")
    scene = QtWidgets.QGraphicsScene()
    ui.graphicsView.setScene(scene)
    return


def actionEjEq_1_f():
    clean_scr()
    ui.lineA.setText("1,2,3,4")
    ui.lineR.setText("(1,1),(2,2),(3,3),(4,4),(1,2),(2,1)")
    return


def actionEjEq_2_f():
    clean_scr()
    ui.lineA.setText("1,2,3,4,5")
    ui.lineR.setText(
        "(1,1),(1,2),(1,5),(2,1),(2,2),(2,5),(3,3),(3,4),(4,3),(4,4),(5,1),(5,2),(5,5)")
    return


def actionEjEq_3_f():
    clean_scr()
    ui.lineA.setText("1,2,3,4,5,6")
    ui.lineR.setText(
        "(1,1),(1,2),(2,1),(2,2),(3,3),(4,4),(4,5),(5,4),(5,5),(6,6)")
    return


def actionEjOP_1_f():
    clean_scr()
    ui.lineA.setText("a,b,c,d,e,f,g,h")
    ui.lineR.setText(
        "(a,a),(b,b),(c,c),(d,d),(e,e),(f,f),(g,g),(h,h),(a,c),(a,d),(a,e),(a,f),(a,g),(a,h),(b,c),(b,d),(b,e),(b,f),(b,g),(b,h),(c,d),(c,e),(c,f),(c,g),(c,h),(d,f),(d,g),(d,h),(e,f),(e,g),(e,h),(f,h),(g,h)")
    return


def actionEjOP_2_f():
    clean_scr()
    ui.lineA.setText("2,4,5,10,12,20,25")
    ui.lineR.setText(
        "(2,2),(2,4),(2,10),(2,12),(2,20),(4,4),(4,12),(4,20),(5,5),(5,10),(5,20),(5,25),(10,10),(10,20),(12,12),(20,20),(25,25)")
    return


def actionEjOP_3_f():
    clean_scr()
    ui.lineA.setText("1,2,4,5,10,20")
    ui.lineR.setText(
        "(1,1),(1,2),(1,4),(1,5),(1,10),(1,20),(2,2),(2,4),(2,10),(2,20),(4,4),(4,20),(5,5),(5,10),(5,20),(10,10),(10,20),(20,20)")
    return


def actionEjX_1_f():
    clean_scr()
    ui.lineA.setText("1,2,3,4,5")
    ui.lineR.setText(
        "(1,1),(1,2),(2,5),(3,3),(3,4),(4,3),(4,4),(5,1),(5,2),(5,5)")
    return


def actionEjX_2_f():
    clean_scr()
    ui.lineA.setText("1,2,3,4,5")
    ui.lineR.setText(
        "(1,1),(1,5),(2,1),(2,2),(3,3),(3,4),(4,3),(4,4),(5,1),(5,2),(5,5)")
    return


def actionEjX_3_f():
    clean_scr()
    ui.lineA.setText("1,2,3,4,5")
    ui.lineR.setText(
        "(1,2),(1,5),(2,1),(2,5),(3,4),(4,3),(5,1),(5,2)")
    return


def format_relation(relation):
    relation = sub('[^A-Za-z0-9,()]+', '',
                      relation).replace("),(", ")_(").split("_")
    for i in range(len(relation)):
        relation[i] = tuple(sub('[^A-Za-z0-9,]+', '',
                                   relation[i]).split(','))  # en strings
    # se convierte la lista en set para evitar pares repetidos
    relation = set(relation)
    return relation


def format_set(set_a):
    A = set(sub('[^A-Za-z0-9,]+', '', set_a).split(","))
    return A


def is_reflexive(A, R):
    res = dict()
    res['reflex'] = 1  # si al final del ciclo queda en 1, si es reflexiva
    res['faltan'] = set()  # guarda las tuplas que faltan para ser reflex
    for element in A:
        par = tuple((element, element))
        if par not in R:
            res['reflex'] = 0
            res['faltan'].add(par)
    return res


def is_symmetric(A, R):
    res = dict()
    res['sim'] = 1  # si al final del ciclo queda en 1, si es simétrica
    res['faltan'] = set()  # guarda las tuplas que faltan para ser simétrica
    for par in R:
        tupla = tuple((par[1], par[0]))
        if tupla not in R:
            res['sim'] = 0
            res['faltan'].add(tupla)
    return res


def is_antisymmetric(A, R):
    res = dict()
    res['anti'] = 1  # si al final del ciclo queda en 1, si es antisimétrica
    res['sobran'] = set()  # guarda las tuplas que faltan para ser antisimétrica
    for par in R:
        tupla = tuple((par[1], par[0]))
        if tupla in R:
            if par[1] != par[0]:
                res['anti'] = 0
                res['sobran'].add(tupla)
    return res


def is_transitive(A, R):
    res = dict()
    res['trans'] = 1  # si al final del ciclo queda en 1, si es transitiva
    res['faltan'] = set()  # guarda las tuplas que faltan para ser transitiva
    for par in R:
        for par_t in R:
            if par[1] == par_t[0]:
                tupla = tuple((par[0], par_t[1]))
                if tupla not in R:
                    res['trans'] = 0
                    res['faltan'].add(tupla)
    return res


def set_tostr(set_a):
    list_of_strings = [str(s) for s in set_a]
    list_of_strings.sort()
    r = "{" + ",".join(list_of_strings).replace("'", "") + "}"
    return r


def poset_edges(A, R):
    R = R.difference(set(tuple((x, x)) for x in A))
    poset = set(R)
    for ab in R:
        for c in A:
            if (ab[1], c) in R and (ab[0], c) in R:
                try:
                    poset.remove((ab[0], c))
                except:
                    pass
    return poset


def identify_routine():

    if ui.lineA.text() == "" or ui.lineR.text() == "":
        ui.statusbar.clearMessage()
        ui.statusbar.showMessage(
            "Para continuar, ingrese elementos en A y tuplas en R.")
    else:
        A = format_set(ui.lineA.text())
        R = format_relation(ui.lineR.text())
        ui.statusbar.showMessage(
            "A = " + set_tostr(A) + ". R = " + set_tostr(R))
        ui.label_head1.setEnabled(True)
        prop_reflex = is_reflexive(A, R)
        prop_sim = is_symmetric(A, R)
        prop_trans = is_transitive(A, R)
        prop_anti = is_antisymmetric(A, R)

        #verifica cada propiedad, si es, lo anuncia, si no, dice quienes faltan
        if prop_reflex['reflex'] == 1:
            ui.label_reflex.setText("R es Reflexiva")
            ui.checkBoxReflex.setEnabled(True)
            ui.checkBoxReflex.setCheckable(True)
            ui.checkBoxReflex.setChecked(True)
            ui.checkBoxReflex.mouseReleaseEvent = lambda event: ui.checkBoxReflex.setChecked(
                True)
        else:
            ui.label_reflex.setText(set_tostr(prop_reflex['faltan']) + " ∉ R")
            ui.checkBoxReflex.setChecked(False)
            ui.checkBoxReflex.setEnabled(True)
            ui.checkBoxReflex.setCheckable(False)

        if prop_sim['sim'] == 1:
            ui.label_sim.setText("R es Simétrica")
            ui.checkBoxSim.setEnabled(True)
            ui.checkBoxSim.setCheckable(True)
            ui.checkBoxSim.setChecked(True)
            ui.checkBoxSim.mouseReleaseEvent = lambda event: ui.checkBoxSim.setChecked(
                True)
        else:
            ui.label_sim.setText(set_tostr(prop_sim['faltan']) + " ∉ R")
            ui.checkBoxSim.setChecked(False)
            ui.checkBoxSim.setEnabled(True)
            ui.checkBoxSim.setCheckable(False)

        if prop_trans['trans'] == 1:
            ui.label_trans.setText("R es Transitiva")
            ui.checkBoxTrans.setEnabled(True)
            ui.checkBoxTrans.setCheckable(True)
            ui.checkBoxTrans.setChecked(True)
            ui.checkBoxTrans.mouseReleaseEvent = lambda event: ui.checkBoxTrans.setChecked(
                True)
        else:
            ui.label_trans.setText(set_tostr(prop_trans['faltan']) + " ∉ R")
            ui.checkBoxTrans.setChecked(False)
            ui.checkBoxTrans.setEnabled(True)
            ui.checkBoxTrans.setCheckable(False)

        if prop_anti['anti'] == 1:
            ui.label_anti.setText("R es Antisimétrica")
            ui.checkBoxAnti.setEnabled(True)
            ui.checkBoxAnti.setCheckable(True)
            ui.checkBoxAnti.setChecked(True)
            ui.checkBoxAnti.mouseReleaseEvent = lambda event: ui.checkBoxAnti.setChecked(
                True)
        else:
            ui.label_anti.setText(set_tostr(prop_anti['sobran']) + " ∈ R")
            ui.checkBoxAnti.setChecked(False)
            ui.checkBoxAnti.setEnabled(True)
            ui.checkBoxAnti.setCheckable(False)

        #determina si es de equivalencia o no
        ui.label_head2.setEnabled(True)

        #Agrego variable para validar si es equivalente y de orden parcial (al mismo tiempo)
        bool_equivalencia = False
        bool_ordenparcial = False

        if prop_reflex['reflex'] == 1 and prop_sim['sim'] == 1 and prop_trans['trans'] == 1:
            ui.label_eq.setText("R es una relación de equivalencia")
            ui.label_head3.setText(
                "Ya que es una relación de Equivalencia, es posible determinar las particiones de R:")
            ui.checkBoxEq.setEnabled(True)
            ui.checkBoxEq.setCheckable(True)
            ui.checkBoxEq.setChecked(True)
            ui.checkBoxEq.mouseReleaseEvent = lambda event: ui.checkBoxEq.setChecked(
                True)
            bool_equivalencia = True
        else:
            # Se borra el texto de que es de orden parcial
            ui.label_eq.setText("")
            # Se deshabilita la carilla de orden parcial
            ui.checkBoxEq.setEnabled(False)
            # Se desmarca la carilla de orden parcial
            ui.checkBoxEq.setChecked(False)

        if prop_reflex['reflex'] == 1 and prop_anti['anti'] == 1 and prop_trans['trans'] == 1:
            ui.label_op.setText("R es una relación de orden parcial")
            ui.label_head3.setText(
                "Ya que es una relación de Orden Parcial, se pueden buscar las cotas superiores e inferiores de R:")
            ui.checkBoxOP.setEnabled(True)
            ui.checkBoxOP.setCheckable(True)
            ui.checkBoxOP.setChecked(True)
            ui.checkBoxOP.mouseReleaseEvent = lambda event: ui.checkBoxOP.setChecked(
                True)
            bool_ordenparcial = True
        else:
            # Se borra el texto de que es de equivalencia
            ui.label_op.setText("")
            # Se deshabilita la carilla de equivalencia
            ui.checkBoxOP.setEnabled(False)
            # Se desmarca la carilla de equivalencia
            ui.checkBoxOP.setChecked(False)

        ui.label_head3.setEnabled(True)
        #Se crea el entorno del grafo
        scene = QtWidgets.QGraphicsScene()
        ui.graphicsView.setScene(scene)
        #color rojo para el círculo
        pen = QtGui.QPen(QtCore.Qt.red)
        #diámetro del círculo
        side = 20

        if bool_equivalencia == True:
            #Se crea un matríz llena de 0
            matriz = zeros((len(A), len(A)))

            #Para cada tupla, se cambia el valor de la matríz de 0 a 1
            for par in R:
                matriz[int(par[1])-1][int(par[0])-1] = 1

            #El ancho y el alto son los valores hara separar los círculos unos de otros
            ancho = 0
            alto = 0
            arrow = ''
            contarrow = 0
            contelement = 0
            elements = dict()
            elements['iden'] = set()
            removed = False

            #Se recorre la nueva matriz
            dibujados = set()
            for i in range(len(A)):
                if i not in dibujados:
                    for j in range(len(A)):
                        contelement = 0
                        for k in range(len(A)):
                            if matriz[i, k] == 1 and i <= k:
                                contelement = contelement+1
                        print(str(contelement)+' '+str(i))
                        if i in elements['iden'] and contelement == 1:
                            print('se quita'+str(contelement)+' '+str(i))
                            removed = True
                        else:
                            removed = False
                        contarrow = 0
                        #Si existe la tupla
                        if (matriz[j, i] == 1 or matriz[i, j] == 1) and (i <= j):
                            if i == j:
                                dibujados.add(i)
                                print(dibujados)
                                #Se dibuja la curva
                                textcurve = scene.addText('∩', QtGui.QFont(
                                    'Arial Black', 10, QtGui.QFont.Light))
                                textcurve.setPos(ancho*side+(ancho*15)+(ancho*4),
                                                 alto*side+(alto*15)-20)
                            else:
                                dibujados.add(j)
                                print(dibujados)
                                if matriz[i, i] == 1 or matriz[j, j] == 1:
                                    #Se dibuja la curva
                                    textcurve = scene.addText('∩', QtGui.QFont(
                                        'Arial Black', 10, QtGui.QFont.Light))
                                    textcurve.setPos(ancho*side+(ancho*15)+(ancho*4),
                                                     alto*side+(alto*15)-20)
                                    elements['iden'].add(j)
                                #Se valida el tipo de nexo (simple o doble)
                                if matriz[i, j] == 1:
                                    contarrow = contarrow + 1
                                if matriz[j, i] == 1:
                                    contarrow = contarrow + 1
                                if contarrow == 2:
                                    arrow = '↔'
                                if contarrow == 1:
                                    arrow = '→'
                                #Se añade la flecha
                                textarrow = scene.addText(arrow, QtGui.QFont(
                                    'Arial Black', 10, QtGui.QFont.Light))
                                textarrow.setPos(ancho*side+(ancho*18) -
                                                 20, alto*side+(alto*15)-7)
                            #Se crean los círculos
                            if (matriz[j, i] == 1 or matriz[i, j] == 1 or i == j) and i <= j:
                                r = QtCore.QRectF(QtCore.QPointF(
                                    ancho*side+(ancho*18), alto*side+(alto*15)), QtCore.QSizeF(side, side))
                                scene.addEllipse(r, pen)
                                textelement = scene.addText(
                                    str(j+1), QtGui.QFont('Arial Black', 10, QtGui.QFont.Light))
                                textelement.setPos(ancho*side+(ancho*18),
                                                   alto*side+(alto*15)-7)
                                ancho = ancho+1
                            arrow = ''
                    if matriz[i, i] == 1 and removed == False:
                        alto = alto+1
                        ancho = 0

        #A partir de aquí puedes ponerle las nuevas líneas de código

        lst = sorted(R)
        part = dict()
        # Se crea una nueva lista vacía para almacenar las identidades [1], [2], etc
        single_part = dict()

        part['tuplas'] = set()  # Se crea una lista vacía
        single_part['tuplas'] = set()  # Se inicializa la lista de identidades
        for par in lst:
            if par[0] == par[1]:  # Se añaden las tuplas
                # Si es lineal se agrega a la nueva lista de identidades
                single_part['tuplas'].add(tuple(par[0]))
            else:
                part['tuplas'].add(tuple(sorted(par)))  # se agrega la tupla

        # Se eliminan los valores duplicados
        mylist = [t for t in (set(tuple(i) for i in part['tuplas']))]
        ant_mylist = mylist

        #para la lista nueva de identidades:
        for unit in sorted(single_part['tuplas']):
            flag = 0  # bandera apagada, se encenderá si el número existe en alguna tupla
            for item in mylist:  # Aquí se valida si está en una tupla
                if unit[0] in item:
                    flag = 1  # Si existe se enciende la bandera

            if flag == 0:  # Después de validar en todas las tuplas, si la bandera sigue abajo
                # Se agrega el valor a la lista principal de particiones
                part['tuplas'].add(tuple(unit[0]))

        # Se vuelven a eliminar los valores duplicados
        mylist = [t for t in (set(tuple(i) for i in part['tuplas']))]

        # Se ordenan de menor a mayor y se enlistan
        list_of_partition = [str(s) for s in sorted(mylist)]

        #se genera un set para la impresión
        if bool_equivalencia == True:
            print_list = mylist
            partitions = list()
            while len(print_list) > 0:
                x = set()
                m = print_list.pop(0)
                for n in m:
                    x.add(n)
                    for element in A:
                        if (n, element) in print_list:
                            x.add(element)
                            print_list.remove((n, element))
                        if (element, n) in print_list:
                            x.add(element)
                            print_list.remove((element, n))
                partitions.append(x)

            #Se agrega al textedit remplazando los caracteres por los que se ocuparán
            ui.textEdit_part.setText("R cuenta con las siguientes clases de equivalencia: \n" + ",".join(list_of_partition).replace(
                "'", "").replace("(", "[").replace(")", "]").replace(",]", "]"))
            #Impresión en formato de la clase
            ui.textEdit_part.append("De esto obtenemos:")
            for p in partitions:
                p = list(sorted(p))
                for element in p:
                    ui.textEdit_part.append(
                        "[" + element + "] = " + set_tostr(p))
            #Impresión de equivalencias
            ui.textEdit_part.append("Es decir:")
            for p in partitions:
                p = list(sorted(p))
                ui.textEdit_part.append("[" + "] = [".join(list(p)) + "]")
            #Impresión de particiones
            ui.textEdit_part.append("Por lo tanto:")
            list_of_partition = [set_tostr(s)
                                 for s in list(sorted(partitions))]
            ui.textEdit_part.append(
                "P = {" + ",".join(list_of_partition) + "}")

        else:
            ui.textEdit_part.setText("")

        #Se determina si forma una lattice
        if bool_ordenparcial == True:
            A_1 = list(A)
            cotas_sup = dict()
            count_mcs = 0
            mcs_exist = 0

            for n in range(0, len(A_1)-1):
                for m in range(n+1, len(A_1)):
                    t = (A_1[n], A_1[m])
                    cotas_sup[t] = dict()
                    cotas_sup[t]['cs'] = list()
                    cotas_sup[t]['mcs'] = list()
                    for x in A_1:
                        if (A_1[n], x) in R and (A_1[m], x) in R:
                            #print('CS{{{0},{1}}} = {2}'.format(A[n], A[m], x))
                            cotas_sup[t]['cs'].append(x)

                    for i in range(0, len(cotas_sup[t]['cs'])):
                        band = 1
                        for j in range(0, len(cotas_sup[t]['cs'])):
                            if i != j:
                                u = tuple(
                                    (cotas_sup[t]['cs'][i], cotas_sup[t]['cs'][j]))
                                if u not in R:
                                    #print (cotas_sup[t]['cs'][i] + " no es")
                                    band = 0
                        if band == 1:
                            cotas_sup[t]['mcs'].append(cotas_sup[t]['cs'][i])
                            count_mcs += 1
                    #Se imprimen las cotas superiores
                    ui.textEdit_part.append('CS{{{0},{1}}} = {2}\t MCS{{{0},{1}}} = {3}'.format(
                        A_1[n], A_1[m], set_tostr(cotas_sup[t]['cs']), set_tostr(cotas_sup[t]['mcs'])))
            if len(cotas_sup) == count_mcs:
                mcs_exist = 1
                ui.textEdit_part.append(
                    "\nExiste una mínima cota superior para cada par x,y ∈ A\n")
            else:
                mcs_exist = 0
                ui.textEdit_part.append(
                    "\nNo existen una mínima cota superior para cada par x,y ∈ A\n")

            cotas_inf = dict()
            count_mci = 0
            mci_exist = 0
            for n in range(0, len(A_1)-1):
                for m in range(n+1, len(A_1)):
                    t = (A_1[n], A_1[m])
                    cotas_inf[t] = dict()
                    cotas_inf[t]['ci'] = list()
                    cotas_inf[t]['mci'] = list()
                    for x in A_1:
                        if (x, A_1[n]) in R and (x, A_1[m]) in R:
                            cotas_inf[t]['ci'].append(x)
                    for i in range(0, len(cotas_inf[t]['ci'])):
                        band = 1
                        for j in range(0, len(cotas_inf[t]['ci'])):
                            if i != j:
                                u = tuple(
                                    (cotas_inf[t]['ci'][j], cotas_inf[t]['ci'][i]))
                                if u not in R:
                                    band = 0
                        if band == 1:
                            cotas_inf[t]['mci'].append(cotas_inf[t]['ci'][i])
                            count_mci += 1

                    #Se imprimen las cotas inferiores
                    ui.textEdit_part.append('CI{{{0},{1}}} = {2}\t MCI{{{0},{1}}} = {3}'.format(
                        A_1[n], A_1[m], set_tostr(cotas_inf[t]['ci']), set_tostr(cotas_inf[t]['mci'])))

            if len(cotas_inf) == count_mci:
                ui.textEdit_part.append(
                    "\nExiste una máxima cota inferior para cada par x,y ∈ A\n")
                mci_exist = 1
            else:
                ui.textEdit_part.append(
                    "\nNo existe una máxima cota inferior para cada par x,y ∈ A\n")
                mci_exist = 0

            if mcs_exist and mci_exist:
                ui.textEdit_part.append("\nPor lo tanto, R es una retícula\n")
                ret = 1
            else:
                ui.textEdit_part.append(
                    "\nPor lo tanto, R no es una retícula\n")
                ret = 0

            #Graficación del poset
            R1 = poset_edges(A, R)
            if ret == 1:
                G = nx.Graph()
            else:
                G = nx.DiGraph()

            G.add_nodes_from(list(A))
            G.add_edges_from(list(R1))

            #se descompone la función ax = plt.figure().gca() para obtener la figura a parte
            fig = figure()  # se almacena la figura en una variable
            ax = fig.gca()
            options = {'node_size': 300,
                       'node_color': 'mediumseagreen', 'with_labels': True}
            nx.draw_spring(G, **options)
            #Se crea un canvas (lienzo) y se le integra la figura
            canvas = FigureCanvas(fig)
            #Se asigna el tamaño y la posición del canvas
            canvas.setGeometry(0, 0, 360, 450)
            #Se añade el canvas a la ventana principal
            scene.addWidget(canvas)
            #plt.show()  # display #comento el display para ya no abrir el grafo en una nueva ventana
    return


def action_binding():
    ui.actionEjEq_1.triggered.connect(actionEjEq_1_f)
    ui.actionEjEq_2.triggered.connect(actionEjEq_2_f)
    ui.actionEjEq_3.triggered.connect(actionEjEq_3_f)
    ui.actionEjOP_1.triggered.connect(actionEjOP_1_f)
    ui.actionEjOP_2.triggered.connect(actionEjOP_2_f)
    ui.actionEjOP_3.triggered.connect(actionEjOP_3_f)
    ui.actionEjX_1.triggered.connect(actionEjX_1_f)
    ui.actionEjX_2.triggered.connect(actionEjX_2_f)
    ui.actionEjX_3.triggered.connect(actionEjX_3_f)
    ui.actionLmpiar.triggered.connect(clean_scr)
    ui.actionSalir.triggered.connect(QtGui.QGuiApplication.exit)
    return


if __name__ == "__main__":
    import sys
    from regex import sub
    from numpy import zeros
    import networkx as nx
    from matplotlib.pyplot import figure
    from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    try:
        MainWindow.setWindowIcon(QtGui.QIcon('icon.png'))
    except:
        pass
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    action_binding()
    ui.statusbar.showMessage(
        "Ingrese elementos en A y tuplas en R separadas por comas, o escoja un ejemplo precargado.")
    ui.buttonIdentificar.clicked.connect(identify_routine)
    MainWindow.show()
    sys.exit(app.exec_())
